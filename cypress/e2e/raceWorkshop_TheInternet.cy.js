describe('template spec', () => {
  beforeEach(() => {
    cy.log('BEFORE EACH METHOD')
    cy.visit('https://the-internet.herokuapp.com/')
  })
  // ADD/REMOVE
  it('Add Elements', () => {
    cy.log('Add/Remove Elements')
    cy.visit('https://the-internet.herokuapp.com/add_remove_elements/')
    cy.get('button').click()
    cy.get('.added-manually').should('have.length', 1)
  })

  it('Remove Elements', () => {
    cy.log('Add/Remove Elements')
    cy.visit('https://the-internet.herokuapp.com/add_remove_elements/')
    cy.get('button').click()
    cy.get('.added-manually').click()
    cy.get('.added-manually').should('have.length', 0)
  })

  // CHECKBOXES
  it('Checkboxes checking', () => {
    cy.visit('https://the-internet.herokuapp.com/checkboxes')
    cy.get('input[type="checkbox"]').check().should('be.checked')

  })
  it('Checkboxes un-checking', () => {
    cy.visit('https://the-internet.herokuapp.com/checkboxes')
    cy.get('input[type="checkbox"]').uncheck().should('not.be.checked')
  })

  // DROPDOWN LIST
  it('Dropdown List', () => {
    cy.visit('https://the-internet.herokuapp.com/dropdown')
    cy.get('select')
      .select(1)
    cy.get('select')
      .should('have.value', '1')
  })
  it('Dropdown List', () => {
    cy.visit('https://the-internet.herokuapp.com/dropdown')
    cy.get('select').select(1)
    cy.get('select').should('not.have.value', '2')
  })

  it('Dropdown List', () => {
    cy.visit('https://the-internet.herokuapp.com/dropdown')
    cy.get('select')
      .select(1)
    cy.get('select')
      .find('option')
      .contains('Please select an option')
      .should('have.attr', 'disabled');
  })

  // KEY PRESSES

  it('Filling in input 32', () => {
    cy.visit('https://the-internet.herokuapp.com/key_presses')
    cy.get('input[type="text"]')
      .type('m')
    cy.get('p').should('contain', 'You entered: M')
  })
  it('Filling in input 32', () => {
    cy.visit('https://the-internet.herokuapp.com/key_presses')
    cy.get('input[type="text"]')
      .type('m')
    cy.get('p').should('not.contain', 'You entered: C')
  })

  // EMAIL
  it('Filling in input 32', () => {
    cy.visit('https://the-internet.herokuapp.com/key_presses')
    cy.get('email')
      .type('kleineHerman@plopsaland.be')
    cy.get('p').should('not.contain', 'You entered: C')
  })
})